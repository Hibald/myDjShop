from django.apps import AppConfig

class ProductAppConfig(AppConfig):
    name = "products"  # Здесь указываем исходное имя приложения
    verbose_name = "Товары" # А здесь, имя которое необходимо отобразить в админке
