from django.db import models


class ProductCategory(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None, verbose_name='Имя')
    is_active = models.BooleanField(default=True, verbose_name='Активен')

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name = 'Категория товара'
        verbose_name_plural = 'Категория товаров'


class Product(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None, verbose_name='Имя')
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Цена')
    discount = models.IntegerField(default=0, verbose_name='Скидка')
    category = models.ForeignKey(ProductCategory, blank=True, null=True, default=None, verbose_name='Категория')
    short_description = models.TextField(blank=True, null=True, default=None, verbose_name='Краткое описание')
    description = models.TextField(blank=True, null=True, default=None, verbose_name='Описание')
    is_active = models.BooleanField(default=True, verbose_name='Активен')
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return "{} {}".format(self.price, self.name)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class ProductImage(models.Model):
    product = models.ForeignKey(Product, blank=True, null=True, default=None, verbose_name='Товар')
    image = models.ImageField(upload_to='products_images/', verbose_name='Путь')
    is_main = models.BooleanField(default=False, verbose_name='Главное фото')
    is_active = models.BooleanField(default=True, verbose_name='Активен')
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return "{}".format(self.id)

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
