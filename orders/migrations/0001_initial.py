# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-05-23 01:26
from __future__ import unicode_literals

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('products', '0007_auto_20170520_1319'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_name', models.CharField(blank=True, default=None, max_length=128, null=True, verbose_name='Имя')),
                ('customer_email', models.EmailField(blank=True, default=None, max_length=254, null=True, verbose_name='Почтовый адрес')),
                ('customer_phone', models.CharField(blank=True, default=None, max_length=48, null=True, verbose_name='Телефон')),
                ('customer_address', models.CharField(blank=True, default=None, max_length=128, null=True, verbose_name='Адрес')),
                ('total_price', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Итого')),
                ('comments', models.TextField(blank=True, default=None, null=True, verbose_name='Примечания')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
        migrations.CreateModel(
            name='ProductInBasket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nmb', models.IntegerField(default=1, verbose_name='Количество')),
                ('price_per_item', models.DecimalField(decimal_places=2, default=Decimal('0'), max_digits=10, verbose_name='Стоимость предмета')),
                ('total_price', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Стоимость всех предметов позиции')),
                ('is_active', models.BooleanField(default=True, verbose_name='Статус')),
                ('session_key', models.CharField(blank=True, default=None, max_length=128, null=True, verbose_name='Ключ Сессии')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('order', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='orders.Order', verbose_name='Заказ')),
                ('product', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.Product', verbose_name='Товар')),
            ],
            options={
                'verbose_name': 'Товар в корзине',
                'verbose_name_plural': 'Товары в корзине',
            },
        ),
        migrations.CreateModel(
            name='ProductInOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nmb', models.IntegerField(default=1, verbose_name='Количество')),
                ('price_per_item', models.DecimalField(decimal_places=2, default=Decimal('0'), max_digits=10, verbose_name='Стоимость предмета')),
                ('total_price', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Стоимость всех предметов позиции')),
                ('is_active', models.BooleanField(default=True, verbose_name='Статус')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('order', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='orders.Order', verbose_name='Заказ')),
                ('product', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.Product', verbose_name='Товар')),
            ],
            options={
                'verbose_name': 'Товар',
                'verbose_name_plural': 'Товары',
            },
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=24, null=True, verbose_name='Имя')),
                ('is_active', models.BooleanField(default=True, verbose_name='Статус')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
            ],
            options={
                'verbose_name': 'Статус заказа',
                'verbose_name_plural': 'Статусы заказа',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.Status', verbose_name='Статус'),
        ),
    ]
