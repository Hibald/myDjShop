$(document).ready(function(){
  var form = $('#form_buying_product');
  // console.log(form);


  function basketUpdating(product_id, nmb, is_delete=false){

      var data = {};
      data.product_id = product_id;
      data.nmb = nmb;


      // var csrf_token = $('#csrf_getting_form [name="csrfmiddlewaretoken"]').val();
      var csrf_token = $('#form_buying_product [name="csrfmiddlewaretoken"]').val();
      data['csrfmiddlewaretoken'] = csrf_token;

      if (is_delete){
        data["is_delete"] = true;
      }

      var url = form.attr("action");
      console.log(data);
      $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data){
          console.log("OK");
          console.log(data.products_total_nmb);
          if (data.products_total_nmb || data.products_total_nmb==0){
            $('#basket_total_nmb').text("("+data.products_total_nmb+")");
            console.log(data.products);
            $('.basket-items ul').html('');

            $.each(data.products, function(k, v){
              $('.basket-items ul').append('<li>' + v.name + ', ' + v.nmb + 'шт. ' + 'по ' + v.price_per_item +  " грн. " +
                '<a href="" class="delete-item" data-product_id="'+v.id+'">x</a>' +
                '</li>');

            }
            );}
        },
        error: function(){
          console.log("error")
        }
          });

  }



  form.on('submit', function(e){
    e.preventDefault();
    // console.log('123');
    var nmb = $('#number').val();
    var submit_btn = $('#submit_btn');
    var product_id = submit_btn.data("product_id");
    var product_name = submit_btn.data("product_name");
    var product_price = submit_btn.data("product_price");
    var product_total = parseFloat(product_price)  * parseFloat(nmb);



    // console.log(product_name +" c id: " + +product_id + " куплено: "+ +nmb + "шт." + " по цене: " + product_total + " грн.");

    // AJAX BASIC EXAMPLE

    // var data = {};
    // var csrf_token = $('#csrf_getting_form [name="csrfmiddlewaretoken"]').val();
    // data['csrfmiddlewaretoken'] = csrf_token;
    // var url = "";
    // $.ajax({
    //   url: url,
    //   data: data,
    //   cache: true,
    //   success: function (data){
    //     console.log("OK");
    //   }
    //   error: function(){
    //     console.log("error")
    //   }
    // });

    basketUpdating(product_id, nmb, is_delete=false);


  });





  function shovingBasket(){
      $('.basket-items').removeClass('hidden');
  }


  $('.basket-container').on('click', function(e){
    e.preventDefault();
    shovingBasket();
  });

  $('.basket-container').mouseover(function(e){
    shovingBasket();
  });

  $('.basket-container').mouseout(function(e){

    shovingBasket();
  });


  $(document).on('click', '.delete-item', function(e){
    e.preventDefault();
    product_id = $(this).data('product_id');
    nmb = 0;
    basketUpdating(product_id, nmb, is_delete=true);
  })
})
