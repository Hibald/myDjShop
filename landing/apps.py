from django.apps import AppConfig



class LandingAppConfig(AppConfig):
    name = "landing"  # Здесь указываем исходное имя приложения
    verbose_name = "Лендинг" # А здесь, имя которое необходимо отобразить в админке
