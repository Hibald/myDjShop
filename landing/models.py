from django.db import models

class Subscriber(models.Model):
    email = models.EmailField(verbose_name='Почтовый адрес')
    name = models.CharField(max_length=128,  verbose_name='Имя')

    def __str__(self):

        return "Id: {} {}".format(self.id, self.name)

    class Meta:
        verbose_name = 'Подписчик'
        verbose_name_plural = 'Подписчики'
